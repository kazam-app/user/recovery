/**
 * Application functionality
 * @file app.js
 */

'use strict'

const key = 'token'

let app = {
  form: null,
  token: null,
  container: null,
  init: function () {
    this.initToken()
    this.initContainer()
    this.initForm()
  },
  initContainer: function () {
    this.container = $('#message-container')
  },
  initToken: function () {
    this.token = this.getParameterByName(key)
  },
  initForm: function () {
    this.form = $('#form-container')
    this.form.submit(this.submit.bind(this))
    if (this.token) this.form.show()
  },
  getParameterByName: function (name) {
    let match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search)
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '))
  },
  submit: function (e) {
    e.preventDefault()
    if (this.token) {
      let data = this.form.serialize()
      data += '&token=' + encodeURIComponent(this.token)
      $.post(
        config.url + '/recover',
        data,
        this.success.bind(this)
      ).fail(this.error.bind(this))
    } else this.form.get(0).reset()
  },
  success: function (data) {
    this.form.hide()
    this.form.get(0).reset()
    this.container.html(
      '<strong>La nueva contrase&ntilde;a ha sido establecida correctamente.<strong>'
    )
  },
  error: function (err) {
    if (err.status === 400) {
      this.form.remove()
      this.container.html(`<strong>${err.responseJSON.message}</strong>`)
    }
  }
}
app.init()
