# Recovery Interface
[![build status](https://git.urbanity.xyz/kazam/user/recovery/badges/master/build.svg)](https://git.urbanity.xyz/kazam/user/recovery/commits/master)

## Dependencies
- yarn
- gulp

## Install

```sh
yarn install
```

## Run

```sh
gulp
```

## Build

```sh
gulp build
```

## Deploy

AWS S3
